#!/bin/bash
​
function llogs() {
  apod=$1
  kpod=$2
  start=$3
  end=$4
  context=$5
​
  if [ -f ~/.outputthistosomewhere ]; then
    rm ~/.outputthistosomewhere
  fi
​
  if [ -z $end ]; then
    end=$start
  fi
  end=$(date -I -d "$end + 1 day")
  start=$(date -I -d "$start")
​
  echo $context
  if [ -z $context ]; then
    context=${axcontext[$apod]}
    if [ -z $context ]; then
      echo "Could not find cluster/context for pod $pod"
      return
    fi
  fi
​
  while [ "$start" != "$end" ]; do
    year=$(date +%Y -d "$start")
    month=$(date +%m -d "$start")
    day=$(date +%d -d "$start")
​
    echo "sudo -u grdndev aws s3 ls s3://ia55-logs/$apod/${axcontext[$apod]}/seed/$year/$month/$day/$kpod --recursive | grep "seed\.[0-9].*\.log""
    sudo -u grdndev aws s3 ls s3://ia55-logs/$apod/$context/seed/$year/$month/$day/$kpod/ --recursive | grep "seed\.[0-9].*\.log" >> ~/.outputthistosomewhere
    start=$(date -I -d "$start + 1 day")
  done
​
  length=$(cat ~/.outputthistosomewhere | head -n 1 | cut -d ' ' -f5  | wc -c)
​
  let char_start=$length-28
​
  cat ~/.outputthistosomewhere | sort -k4.$char_start,4.$length > ~/sortedlogs.txt
​
  rm ~/.outputthistosomewhere
  cat ~/sortedlogs.txt
}
​
function getlogs() {
  file=$1
​
  name=${file: -28}
  echo $name
​
  mkdir -p /tmp/$USER/
  cd /tmp/$USER
  chmod 777 .
​
  if [ -f "/tmp/$USER/$file" ]; then
    echo "File already present"
    return
  fi
​
  echo "sudo -u grdndev aws s3 cp s3://ia55-logs/$1 ."
  sudo -u grdndev aws s3 cp s3://ia55-logs/$1 .
​
  echo "TO COPY RUN BELOW COMMAND IN CODEMILL/WORKSPACES"
  echo "remctl $(hostname) file cat /tmp/$USER/$name > $name"
}
​
declare -A axcontext
​
produseast1prod=(baam baly bayview bcimpl bhsys boothbay brdreach brkfld cert-manager clearalpha default deshaw dragon drw durable ejf firtree firtree-ubor gatekeeper-system golden greenland gtam hbridge jhi jmos jpm jpmctl jpmdemo jpmtfs jref jssdp kodai kube-node-lease kube-public kube-system linkerd logstash-produat lordabbett manteio mio msim opal paamco radix rokos sandbox sapphire secor shared shared-kubecost shared-recon shared-ubor slatepath sram ssa surgo tbird tengyue tig wam wf wq)
​
produseast1uat=(baamuat baamuat-ubor balysandbox balysandbox-ubor balyuat balyuat-ubor cert-manager default deshawuat diamond drwuat firtreeuat firtreeuat-ubor gatekeeper-system gtamuat hbridgeuat jmosuat jpmctluat jpmuat jrefuat jssdpuat kube-node-lease kube-public kube-system linkerd litmus msimuat secoruat shared-uat shared-uat-kubecost shared-uat-recon shared-uat-ubor sramimpl sramuat ssauat wquat)
​
produseast1dev=(arc-ingress-test-1 cert-manager default gatekeeper-system henner-test kube-node-lease kube-public kube-system linkerd litmus logstash-devqa mars mars-armor-etl mars-ubor marsclone phobos phobos-ubor shared-dev shared-dev-infra-argo-events shared-dev-infra-argo-workflows shared-dev-kubecost shared-dev-recon shared-dev-ubor shared-qa shared-qa-recon shared-qa-ubor terra terra-armor-etl terra-infra-eventworkflow-demo terra-ubor)
​
prodapsoutheast2customer=(bjcp bjcpuat cert-manager default gatekeeper-system kube-node-lease kube-public kube-system linkerd logging polaris pseudo-shared shared-kubecost)
​
devhydapsouth1dev=(cert-manager default europa gatekeeper-system jupiter kube-node-lease kube-public kube-system linkerd litmus shared-dev shared-dev-jobcompute shared-dev-kubecost shared-dev-recon)
​
prodapsoutheast1prod=(cert-manager default gatekeeper-system gic gic-ubor gicuat gicuat-ubor kube-node-lease kube-public kube-system linkerd shared shared-kubecost shared-recon shared-uat shared-uat-recon)
​
​
for i in ${produseast1prod[@]}; do
  axcontext[$i]=prod--us-east-1--prod
done
​
for i in ${produseast1uat[@]}; do
  axcontext[$i]=prod--us-east-1--uat
done
​
for i in ${prodapsoutheast2customer[@]}; do
  axcontext[$i]=prod--ap-southeast-2--customer
done
​
for i in ${devhydapsouth1dev[@]}; do
  axcontext[$i]=dev-hyd--ap-south-1--dev
done
​
for i in ${prodapsoutheast1prod[@]}; do
  axcontext[$i]=prod--ap-southeast-1--prod
done
​
for i in ${produseast1dev[@]}; do
  axcontext[$i]=prod--us-east-1--dev
done
