# llogs - List Logs (for SEED)
llogs (list logs) will collect all the SEED log files between the given dates inclusive, sort it based on the time available in the file name and outputs it to your console. 
It also stores the output in sortedlogs.txt in your home directory for you to browse later. 

getlogs will get and store the logs to your /tmp/$USER directory

## Installation
Add the below line to your ~/.bashrc or ~/.zshrc file
``` 
source <path to llgs>/llogs.sh

Example:
source ~/repos/llogs/llogs.sh
```

## Usage
```
$ llogs <apod> <seed instance> <start date> <optional end date>
$ getlogs <s3 path>
```

## Example 

```
$ llogs deshaw seed-2 20230501 20230502

sudo -u grdndev aws s3 ls s3://ia55-logs/deshaw/prod--us-east-1--prod/seed/2023/05/01/seed-2 --recursive | grep seed.[0-9].*.log
sudo -u grdndev aws s3 ls s3://ia55-logs/deshaw/prod--us-east-1--prod/seed/2023/05/02/seed-2 --recursive | grep seed.[0-9].*.log
2023-05-04 16:05:43  536870971 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-04-12-01-52.log
2023-05-04 20:12:59  536871176 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-04-15-50-45.log
2023-05-05 01:38:52  536870992 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-04-20-08-19.log
2023-05-05 05:52:37  536871071 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-05-01-35-12.log
2023-05-05 07:44:33  536879128 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-05-05-39-59.log
2023-05-05 11:44:53  536871043 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-05-07-32-44.log
2023-05-05 15:15:29  536870933 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-05-11-42-34.log
2023-05-05 19:21:45  536871040 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-05-15-07-32.log
2023-05-06 02:27:25  536870930 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-05-19-07-16.log
2023-05-06 18:27:39  536870928 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-06-02-18-29.log
2023-05-07 08:20:16  536871102 deshaw/prod--us-east-1--prod/seed/2023/05/04/seed-2/54f6574a-993c-46ef-9dc1-cd844b3679bf/log-dir/seed.2023-05-06-18-18-24.log

$ getlogs deshaw/prod--us-east-1--prod/seed/2023/05/18/seed-2/8a3aa75a-f885-4b0b-a03d-330940c1d7c3/log-dir/seed.2023-06-02-20-01-18.log
seed.2023-06-02-20-01-18.log

sudo -u grdndev aws s3 cp s3://ia55-logs/deshaw/prod--us-east-1--prod/seed/2023/05/18/seed-2/8a3aa75a-f885-4b0b-a03d-330940c1d7c3/log-dir/seed.2023-06-02-20-01-18.log .
download: s3://ia55-logs/deshaw/prod--us-east-1--prod/seed/2023/05/18/seed-2/8a3aa75a-f885-4b0b-a03d-330940c1d7c3/log-dir/seed.2023-06-02-20-01-18.log to ./seed.2023-06-02-20-01-18.log
TO COPY RUN BELOW COMMAND IN CODEMILL/WORKSPACES
remctl app1.terra.c.ia55.net file cat /tmp/rajendrn/seed.2023-06-02-20-01-18.log > seed.2023-06-02-20-01-18.log
```

